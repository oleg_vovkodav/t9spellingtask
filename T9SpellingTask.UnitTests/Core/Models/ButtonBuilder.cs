﻿namespace T9SpellingTask.UnitTests.Core.Models
{
    using T9SpellingTask.Core.Models;

    public class ButtonBuilder
    {
        private readonly Button _button = new Button();

        public ButtonBuilder WithNumber(int number)
        {
            _button.Number = number;
            return this;
        }

        public ButtonBuilder WithCharacters(char[] characters)
        {
            _button.Characters = characters;
            return this;
        }

        public Button Build()
        {
            return _button;
        }
    }
}
