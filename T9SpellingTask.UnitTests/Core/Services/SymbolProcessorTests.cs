﻿namespace T9SpellingTask.UnitTests.Core.Services
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    
    using T9SpellingTask.Core.Services;

    [TestClass]
    public class SymbolProcessorTests
    {
        private SymbolProcessor _processor;
        private SymbolProcessorTestFixture _testData;

        [TestInitialize]
        public void TestInit()
        {
            _testData = new SymbolProcessorTestFixture();
            _processor = new SymbolProcessor(_testData.AddDefaultButtons().Build());
        }

        [TestMethod]
        public void ParseString_ValidStringAsParam_StringParsed()
        { 
            // Arrange
            string excpected = "44 444";

            // Act
            string actual = _processor.ParseString("hi");

            // Assert
            Assert.AreEqual(actual, excpected);
        }

        [TestMethod]
        public void ParseString_InvalidStringAsParam_EmptyStringReturned()
        {
            // Arrange
            string excpected = string.Empty;

            // Act
            string actual = _processor.ParseString("123");

            // Assert
            Assert.AreEqual(actual, excpected);
        }
    }
}
