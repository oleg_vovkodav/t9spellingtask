﻿namespace T9SpellingTask.UnitTests.Core.Services
{
    using System.Collections.Generic;

    using T9SpellingTask.Core.Contracts;
    using T9SpellingTask.UnitTests.Core.Models;

    public class SymbolProcessorTestFixture
    {
        private readonly IList<IButton> _buttons = new List<IButton>();

        public SymbolProcessorTestFixture AddDefaultButtons()
        {
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(2)
                                .WithCharacters(new char[] { 'a', 'b', 'c' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(3)
                                .WithCharacters(new char[] { 'd', 'e', 'f' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(4)
                                .WithCharacters(new char[] { 'g', 'h', 'i' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(5)
                                .WithCharacters(new char[] { 'j', 'k', 'l' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(6)
                                .WithCharacters(new char[] { 'm', 'n', 'o' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(7)
                                .WithCharacters(new char[] { 'p', 'q', 'r', 's' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(8)
                                .WithCharacters(new char[] { 't', 'u', 'v' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(9)
                                .WithCharacters(new char[] { 'w', 'x', 'y', 'z' })
                                .Build());
            _buttons.Add(new ButtonBuilder()
                                .WithNumber(0)
                                .WithCharacters(new char[] { ' ' })
                                .Build());
            return this;
        }

        public SymbolProcessorTestFixture AddButton(IButton button)
        {
            _buttons.Add(button);
            return this;
        }

        public IEnumerable<IButton> Build()
        {
            return _buttons;
        }
    }
}
