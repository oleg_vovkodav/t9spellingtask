﻿namespace T9SpellingTask.Dal.Services
{
    using System.IO;

    using T9SpellingTask.Dal.Contracts;

    public class FileProcessor : IInputOutputProcesscor
    {
        private string _inputFileName;
        private string _outputFileName;

        public FileProcessor(string inputFile, string outputFile)
        {
            this._inputFileName = inputFile;
            this._outputFileName = outputFile;
        }

        public void OutputData(string s)
        {
            using (var writer = File.AppendText(_outputFileName))
            {
                writer.WriteLine(s);
            }
        }

        public string[] GetInputData()
        {
            return File.ReadAllLines(_inputFileName);
        }
    }
}
