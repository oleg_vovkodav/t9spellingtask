﻿namespace T9SpellingTask.Dal.Contracts
{
    public interface IInputOutputProcesscor
    {
        void OutputData(string s);
        string[] GetInputData();
    }
}
