﻿namespace T9SpellingTask.DependencyResolver
{
    using Ninject.Modules;
    using T9SpellingTask.Dal.Contracts;
    using T9SpellingTask.Dal.Services;

    public class NinjectDalModule : NinjectModule
    {
        private string _inputFileName;
        private string _outputFileName;

        public NinjectDalModule(string inputFile, string outputFile)
        {
            this._inputFileName = inputFile;
            this._outputFileName = outputFile;
        }

        public override void Load()
        {
            Bind<IInputOutputProcesscor>().To<FileProcessor>()
                .WithConstructorArgument("inputFile", this._inputFileName)
                .WithConstructorArgument("outputFile", this._outputFileName);
        }
    }
}
