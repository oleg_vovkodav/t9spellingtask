﻿namespace T9SpellingTask.DependencyResolver
{
    using System.Collections.Generic;

    using Ninject.Modules;
    using T9SpellingTask.Core.Contracts;
    using T9SpellingTask.Core.Services;

    public class NinjectCoreModule : NinjectModule
    {
        private IEnumerable<IButton> _buttons;

        public NinjectCoreModule(IEnumerable<IButton> buttons)
        {
            _buttons = buttons;
        }

        public override void Load()
        {
            Bind<ISpellingService>().To<SpellingService>();
            Bind<ISymbolProcessor>().To<SymbolProcessor>()
                .WithConstructorArgument("buttons", _buttons);
        }
    }
}
