﻿namespace T9SpellingTask.Ui
{
    using System;
    using System.IO;

    using T9SpellingTask.Core.Contracts;

    internal class SpellingController
    {
        private readonly ISpellingService _spellingService;
        private readonly IInputOutputFactory _inputOutputFactory;
        private IInputOutputProcesscor _inputOutputProcessor;

        public SpellingController(ISpellingService spellingService, IInputOutputFactory inputOutputFactory)
        {
            _spellingService = spellingService;
            _inputOutputFactory = inputOutputFactory;
        }

        public void InputOutputProcessorInit()
        {
            string inputFileName = GetInputFileName();

            _inputOutputProcessor = _inputOutputFactory.CreateFileProcessor(inputFileName, "outputNew.dat");
        }

        public void StartService()
        {
            _spellingService.InputData = _inputOutputProcessor.GetInputData();
            _spellingService.Run();
            _inputOutputProcessor.SaveOutputData(_spellingService.GetOutputData);
        }

        private bool isInputFileName(string fileName)
        {
            Console.WriteLine("Please enter input file name.");
            string fileName = Console.ReadLine();
            while (!File.Exists(fileName))
            {
                Console.WriteLine("File {0} doesn't exist, please try again.", Path.GetFileName(fileName));
                fileName = Console.ReadLine();
            }
            return fileName;
        }

        private string GetOutputFileName()
        {
            Console.WriteLine("Please enter output file name.");
            string fileName = Console.ReadLine();
            string response = string.Empty;
            while (File.Exists(fileName))
            {
                Console.WriteLine("File {0} already exists, do you want to overwrite it? (Y/N)", Path.GetFileName(fileName));
                if (overwriteApproved(Console.ReadLine()))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Please type another file name.");
                    fileName = Console.ReadLine();
                }
            }
            return fileName;
        }

        private bool overwriteApproved(string response)
        {
            while (!(response.ToUpperInvariant().Equals("Y")
                || response.ToUpperInvariant().Equals("N")))
            {
                Console.WriteLine("Please answer correctly (Y/N).");
                response = Console.ReadLine();
            }

            return response.ToUpperInvariant().Equals("Y");
        }
    }
}
