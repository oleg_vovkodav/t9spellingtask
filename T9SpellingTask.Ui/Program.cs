﻿namespace T9SpellingTask.Ui
{
    using System.Collections.Generic;

    using Ninject;
    using Ninject.Extensions.Factory;
    using Ninject.Modules;
    using T9SpellingTask.Core.Contracts;
    using T9SpellingTask.Core.Models;
    using T9SpellingTask.Core.Services;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            IKernel kernel = new StandardKernel();

            kernel.Bind<IInputOutputFactory>().ToFactory();
            kernel.Bind<ISymbolProcessor>().To<SymbolProcessor>()
                .WithConstructorArgument("buttons", ButtonsInit());
            kernel.Bind<ISpellingService>().To<SpellingService>();
            kernel.Bind<IInputOutputProcesscor>().To<FileProcessor>();

            var spellingController = kernel.Get<SpellingController>();

            while (true)
            {
                spellingController.InputOutputProcessorInit();
                spellingController.StartService();
            }
        }

        private static IEnumerable<IButton> ButtonsInit()
        {
            return new List<IButton> 
            {
                new Button { Number = 2, Characters = new char[] { 'a', 'b', 'c' } },
                new Button { Number = 3, Characters = new char[] { 'd', 'e', 'f' } },
                new Button { Number = 4, Characters = new char[] { 'g', 'h', 'i' } },
                new Button { Number = 5, Characters = new char[] { 'j', 'k', 'l' } },
                new Button { Number = 6, Characters = new char[] { 'm', 'n', 'o' } },
                new Button { Number = 7, Characters = new char[] { 'p', 'q', 'r', 's' } },
                new Button { Number = 8, Characters = new char[] { 't', 'u', 'v' } },
                new Button { Number = 9, Characters = new char[] { 'w', 'x', 'y', 'z' } },
                new Button { Number = 0, Characters = new char[] { ' ' } }
            };   
        }
    }
}
