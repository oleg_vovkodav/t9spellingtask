﻿namespace T9SpellingTask.Core.Models
{
    using T9SpellingTask.Core.Contracts;

    public class Button : IButton
    {
        public int Number { get; set; }
        public char[] Characters { get; set; }
    }
}
