﻿namespace T9SpellingTask.Core.Contracts
{
    public interface IButton
    {
        int Number { get; set; }
        char[] Characters { get; set; }
    }
}
