﻿namespace T9SpellingTask.Core.Contracts
{
    using System.Collections.Generic;

    public interface IInputOutputProcesscor
    {
        void SaveOutputData(IEnumerable<string> outputData);
        string[] GetInputData();
    }
}
