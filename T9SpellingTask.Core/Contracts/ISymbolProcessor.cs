﻿namespace T9SpellingTask.Core.Contracts
{
    public interface ISymbolProcessor
    {
        string ParseString(string inputString);
    }
}
