﻿namespace T9SpellingTask.Core.Contracts
{
    using T9SpellingTask.Core.Contracts;

    public interface IInputOutputFactory
    {
        IInputOutputProcesscor CreateFileProcessor(string inputFile, string outputFile);
    }
}
