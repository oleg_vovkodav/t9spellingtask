﻿namespace T9SpellingTask.Core.Contracts
{
    using System.Collections.Generic;

    public interface ISpellingService
    {
        IEnumerable<string> GetOutputData { get; }
        IEnumerable<string> InputData { get; set; }
        void Run();
    }
}
