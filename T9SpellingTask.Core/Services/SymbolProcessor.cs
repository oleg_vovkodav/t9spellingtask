﻿namespace T9SpellingTask.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using T9SpellingTask.Core.Contracts;

    /// <summary>
    /// Defines SymbolProcessor.
    /// </summary>
    public class SymbolProcessor : ISymbolProcessor
    {
        /// <summary>
        /// Holds collection of Button instances.
        /// </summary>
        private readonly IEnumerable<IButton> _buttons;

        /// <summary>
        /// Holds StringBuilder instance.
        /// </summary>
        private readonly StringBuilder _stringBuilder;

        /// <summary>
        /// Holds array of symbols which are allowed for processing.
        /// </summary>
        private readonly char[] _allowedSymbols;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymbolProcessor"/> class
        /// </summary>
        /// <param name="buttons">Buttons collection.</param>
        public SymbolProcessor(IEnumerable<IButton> buttons)
        {
            _stringBuilder = new StringBuilder();
            _buttons = buttons;
            _allowedSymbols = GetAllowedSymbols();
        }

        /// <summary>
        /// Parses string according to provided button collection.
        /// </summary>
        /// <param name="inputString">String to parse.</param>
        /// <returns>Parsed string.</returns>
        public string ParseString(string inputString)
        {
            char[] symbols = inputString.ToCharArray();
            if (AreValidCharacters(symbols))
            {
                _stringBuilder.Clear();
                IButton currentButton = null;

                foreach (char symbol in symbols)
                {
                    if (Equals(currentButton, CurrentButton(symbol)))
                    {
                        _stringBuilder.Append(' ');
                    }
                    _stringBuilder.Append(ParseSymbol(symbol));
                    currentButton = CurrentButton(symbol);
                }

                return _stringBuilder.ToString(); 
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Parses particular symbol into representing number.
        /// </summary>
        /// <param name="symbol">Symbol to parse.</param>
        /// <returns>Number which perpresents symbol.</returns>
        private int ParseSymbol(char symbol)
        {
            var button = _buttons.FirstOrDefault(b => b.Characters.Contains(symbol));
            int result = 0;
            int index = Array.IndexOf(button.Characters, symbol);
            for (int i = 0; i <= index; i++)
            {
                result += button.Number * (int)Math.Pow(10, i);
            }

            return result;
        }

        private IButton CurrentButton(char symbol)
        {
            return _buttons.FirstOrDefault(b => b.Characters.Contains(symbol));
        }

        private bool AreValidCharacters(char[] characters)
        {
            return !characters.Except(_allowedSymbols).Any();
        }

        private char[] GetAllowedSymbols()
        {
            foreach (var button in _buttons)
            {
                _stringBuilder.Append(button.Characters);
            }

            return _stringBuilder.ToString().ToCharArray();
        }
    }
}
