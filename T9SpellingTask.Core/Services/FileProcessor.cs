﻿namespace T9SpellingTask.Core.Services
{
    using System.Collections.Generic;
    using System.IO;

    using T9SpellingTask.Core.Contracts;

    public class FileProcessor : IInputOutputProcesscor
    {
        private string _inputFileName;
        private string _outputFileName;

        public FileProcessor(string inputFile, string outputFile)
        {
            this._inputFileName = inputFile;
            this._outputFileName = outputFile;
        }

        public void SaveOutputData(IEnumerable<string> outputData)
        {
            File.WriteAllLines(_outputFileName, outputData);
        }

        public string[] GetInputData()
        {
            return File.ReadAllLines(_inputFileName);
        }
    }
}
