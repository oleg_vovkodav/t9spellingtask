﻿namespace T9SpellingTask.Core.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using T9SpellingTask.Core.Contracts;

    public class SpellingService : ISpellingService
    {
        private readonly ISymbolProcessor _symbolProcessor;
        private IList<string> _inputData;
        private IEnumerable<string> _outputData;

        public SpellingService(ISymbolProcessor symbolProcessor)
        {
            this._symbolProcessor = symbolProcessor;
        }

        IEnumerable<string> ISpellingService.GetOutputData
        {
            get { return _outputData; }
        }

        public IEnumerable<string> InputData
        {
            get
            {
                return _inputData;
            }
            set
            {
                _inputData = value.Skip(1).ToList();
            }
        }

        public void Run()
        {
            if (_inputData != null)
            {
                _outputData = _inputData.Select(
                                        s => AddStringHeader(_inputData.IndexOf(s) + 1) + _symbolProcessor.ParseString(s));
            }
            else
            {
                throw new InvalidOperationException("There are no data to process. You must provide data for process first.");
            }
        }

        private string AddStringHeader(int index)
        {
            return string.Format("Case #{0}: ", index);
        }
    }
}
